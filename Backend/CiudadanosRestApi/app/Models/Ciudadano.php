<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ciudadano extends Model
{
    use HasFactory;
    protected $fillable = [
        'nro_documento',
        'nombre',
        'paterno',
        'materno',
        'telefono',
        'direccion',
      ];

}
