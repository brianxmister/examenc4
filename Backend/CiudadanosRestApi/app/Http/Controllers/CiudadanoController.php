<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ciudadano;
class CiudadanoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
      

        try {
            $posts = Ciudadano::all();
        } catch(\Exception $e) {
            return $this->respondWithError(['message_error' => 'Hubo un error al registrarse.']);
        }

        return response()->json($posts, 201);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
    public function register(Request $request)
    {
        $request->validate([
            'nro_documento' => 'required',
            'nombre' => 'required',
            'paterno' => 'required',
            'materno' => 'required',

        ]);
        try {
            Ciudadano::create($request->all());
        } catch(\Exception $e) {
            return $this->respondWithError(['message_error' => 'Hubo un error al registrarse.']);
        }

        return response()->json('Ciudadano registrado existosamente', 201);
        
    }
}
